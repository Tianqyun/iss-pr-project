from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from AdudittingSystem.sensitive_word_detection import SENSITIVE_WORD_DETECTION
from AdudittingSystem.forbidden_face_api import FORIDDEN_FACE_API
from AdudittingSystem.video_slide import VIDEO_SLIDE, get_time, merge
import json
import opennsfw2 as n2
# Create your views here.


def detect_sentence(request):

    if request.method == "GET":
        string = request.GET.get('input')
        dic = SENSITIVE_WORD_DETECTION(string)
        ans = {"status": True, "code": 200, "data": []}
        for key in dic.keys():
            ans["data"].append({"name": key, "times": dic[key]})
        ans = JsonResponse(ans, safe=False)
        return ans


def picture_detection(request):
    if request.method == "GET":
        path = request.GET.get('location')
        dic = FORIDDEN_FACE_API(path)
        ans = {"status": True, "code": 200, "data": []}
        for key in dic.keys():
            ans["data"].append({"name": key, "times": dic[key]})
        ans = JsonResponse(ans, safe=False)
        return ans


def pornography_detection(request):
    if request.method == "GET":
        path = request.GET.get('location')
        dic = n2.predict_image(path)
        ans = {"status": True, "code": 200, "data": []}
        typ = ""
        if dic >= 0.95:
            typ = "porn"
        elif 0.75 <= dic < 0.95:
            typ = "possible pornography, need to be checked manually"
        else:
            typ = "normal picture"
        ans["data"].append({"probability": dic, "type": typ})
        ans = JsonResponse(ans, safe=False)
        return ans


def video_detection(request):
    if request.method == "GET":
        path = request.GET.get('location')
        pathdir = VIDEO_SLIDE(path)
        dic = {}
        typ = 0
        for pathdi in pathdir:
            typ = max(n2.predict_image(pathdi), typ)
            dic = merge(dic, FORIDDEN_FACE_API(pathdi))
        if typ >= 0.95:
            typ = "porn"
        elif 0.75 <= typ < 0.95:
            typ = "possible pornography, need to be checked manually"
        else:
            typ = "normal video"
        ans = {"status": True, "code": 200, "data": [], "type": typ}
        for key in dic.keys():
            ans["data"].append({"name": key, "times": dic[key]})
        ans = JsonResponse(ans, safe=False)
        return ans


def admin(request):
    return HttpResponse("hello")




