from django.db import models

class Content(models.Model):
    # Field name made lowercase.
    id = models.BigIntegerField(db_column='id', primary_key=True)
    # Field name made lowercase.
    typeId = models.IntegerField(db_column='typeId')
    # Field name made lowercase.
    userId = models.ForeignKey('User', models.DO_NOTHING, db_column='userId')
    # Field name made lowercase.
    fromResource = models.CharField(db_column='fromResource', max_length=50)
    # Field name made lowercase.
    mediaTitle = models.CharField(
        db_column='mediaTitle', max_length=20, blank=True, null=True)
    # Field name made lowercase.
    checkResult = models.IntegerField(db_column='checkResult')
    # Field name made lowercase.
    lableFromModel = models.IntegerField(db_column='lableFromModel')
    url = models.CharField(max_length=100, blank=True, null=True)
    # Field name made lowercase.
    frameUrl = models.CharField(
        db_column='frameUrl', max_length=100, blank=True, null=True)
    strings = models.CharField(max_length=400, blank=True, null=True)
    # Field name made lowercase.
    createTime = models.DateTimeField(
        db_column='createTime', blank=True, null=True)
    # Field name made lowercase.
    updateTime = models.DateTimeField(
        db_column='updateTime', blank=True, null=True)
    is_deleted = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'content'


class User(models.Model):
    # Field name made lowercase.
    id = models.BigIntegerField(db_column='id', primary_key=True)
    # Field name made lowercase.
    userName = models.CharField(db_column='userName', max_length=8)
    # Field name made lowercase.
    passWord = models.CharField(db_column='passWord', max_length=16)
    email = models.CharField(max_length=50, blank=True, null=True)
    # Field name made lowercase.
    roleId = models.IntegerField(db_column='roleId', blank=True, null=True)
    # Field name made lowercase.
    inWhiteList = models.IntegerField(db_column='inWhiteList')
    is_deleted = models.IntegerField(blank=True, null=True)
    # Field name made lowercase.
    createTime = models.DateTimeField(
        db_column='createTime', blank=True, null=True)
    # Field name made lowercase.
    updateTime = models.DateTimeField(
        db_column='updateTime', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user'


class Verifyhistory(models.Model):
    # Field name made lowercase.
    id = models.BigIntegerField(db_column='id', primary_key=True)
    # Field name made lowercase.
    userId = models.ForeignKey(
        User, models.DO_NOTHING, db_column='userId', blank=True, null=True)
    # Field name made lowercase.
    contentId = models.ForeignKey(
        Content, models.DO_NOTHING, db_column='contentId', blank=True, null=True)
    reason = models.CharField(max_length=50, blank=True, null=True)
    # Field name made lowercase.
    createTime = models.DateTimeField(
        db_column='createTime', blank=True, null=True)
    # Field name made lowercase.
    updateTime = models.DateTimeField(
        db_column='updateTime', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'verifyhistory'
