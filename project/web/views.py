from django.shortcuts import HttpResponse
import json
from web.utils.dataUtils import userModel, contentModel
from web.utils.dataUtils import model2dict, QuerySet2ModelList, \
    Query2Dict, Query2DictList, myResponseDict, contentWithUser_Query2DictList, contentWithUser_Query2Dict
from django.http import JsonResponse
from django.core import serializers
# from django.forms.models import model_to_dict 不能用

# Create your views here.

def listAllUsers(request):

    return HttpResponse('login error')

def userNameLogin(request):
    if request.method == 'GET':
        userName = request.GET.get('userName')
        passWord = request.GET.get('passWord')

        usersQuery=userModel.filter(userName=userName,passWord=passWord)
        data = Query2Dict(usersQuery)
        print(type(data))
        
        # 序列化与反序列化，是对应json<=>model对象的，model对象有一些奇奇怪怪的属性，要用dumps
        # request.session['user'] = serializers.serialize('json',QuerySet2List(users))
        # 因为保存的时候就是对应的model类型的字符串，所以最后load的时候也是model
        request.session['user'] = json.dumps(data)
        print(request.session['user'])
        
        res=myResponseDict('',data)
        res = JsonResponse(res, safe=False)
        
        return res
        
    return HttpResponse('login error')


def getLoginUser(request):
    jsonUser=request.session['user']
    if jsonUser == '':
        res=myResponseDict('false', '')
        return JsonResponse(res,safe=False)
    data=json.loads(jsonUser)

    res=myResponseDict('',data)
    return JsonResponse(res,safe=False)

def getContentByTypeId(request):
    typeId=request.GET.get('typeId')
    contentsQuery = contentModel.filter(
        typeId=typeId, checkResult=0).select_related('userId')
    
    # data = Query2DictOrDictList(contentsQuery)
    data = contentWithUser_Query2DictList(contentsQuery)
    res=myResponseDict('',data)
    return JsonResponse(res,safe=False)
    
    
    