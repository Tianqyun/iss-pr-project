from lib2to3.pgen2.pgen import DFAState
from web import models
import json
from django.core import serializers
from django.db.models.fields import DateTimeField, DateField, TimeField, DecimalField
from django.db.models.fields.related import ManyToManyField, ForeignKey

userModel=models.User.objects
contentModel=models.Content.objects


def QuerySet2ModelList (queryset):
    list=[]
    for item in queryset:
        list.append(item)
    return list


def model2dict(obj, fields=None, exclude=None):
    """
    将 Model 对象 obj 转化为字典格式返回
    :param obj: 通过 model.objects.filter().first() 、model.objects.get() 或者 for i in QuerySet 获取的对象
    :param fields: 要选择性呈现的字段
    :param exclude: 排除呈现的字段
    :return: 返回字典 {'id': 1, 'field1': 'field1_data'}
    """
    data = {}
    for f in getattr(obj, '_meta').concrete_fields + getattr(obj, '_meta').many_to_many:
        value = f.value_from_object(obj)

        if fields and f.name not in fields:
            continue

        if exclude and f.name in exclude:
            continue

        if isinstance(f, ManyToManyField):
            value = [i.id for i in value] if obj.pk else None

        elif isinstance(f, DateTimeField):
            value = value.strftime('%Y-%m-%d %H:%M:%S') if value else None

        elif isinstance(f, DateField):
            value = value.strftime('%Y-%m-%d') if value else None

        elif isinstance(f, TimeField):
            value = value.strftime('%H:%M:%S') if value else None

        # elif isinstance(f, DecimalField):
        #     value = float_value(f)

        # ForeignKey 特殊处理
        if isinstance(f, ForeignKey):
            data[f.column] = value
            data[f.name] = value
        else:
            data[f.name] = value

    # # 获取 property 里面的数据
    # for p in getattr(getattr(obj, '_meta'), '_property_names'):
    #     value = getattr(obj, p)
    #     if isinstance(value, (str, int, Decimal)):
    #         data[p] = value
    return data


def Query2Dict(queryset):
    modelList = QuerySet2ModelList(queryset)
    if len(modelList)==1:
        dict=model2dict(modelList[0])
    return dict


def Query2DictList(queryset):
    modelList = QuerySet2ModelList(queryset)
    dictList = []
    for model in modelList:
        a = model2dict(model)
        dictList.append(a)
    return dictList


def myResponseDict(message, data, status=True, code=0):
    res = {"status": status, "code": code, "message": message,"data": data}
    return res


def contentWithUser_Query2DictList(contentsQuery):
    contents= Query2DictList(contentsQuery)
    print(type(contents))
    if type(contents)==dict:        
        contents=letContentWithUser(contents)
        
    if type(contents)==list:
        for i in range(0,len(contents)):
            contents[i]=letContentWithUser(contents[i])
    return contents   


def contentWithUser_Query2Dict(contentsQuery):
    contents = Query2Dict(contentsQuery)
    print(type(contents))
    if type(contents) == dict:
        contents = letContentWithUser(contents)

    if type(contents) == list:
        for i in range(0, len(contents)):
            contents[i] = letContentWithUser(contents[i])
    return contents
        
def letContentWithUser(content):
        userId = content['userId']
        userQuery = userModel.filter(id=userId).values(
            'userName', 'email', 'roleId', 'inWhiteList')
        # user=Query2DictOrDictList(userQuery)
        print(type(userQuery),userQuery)
        user = userQuery[0]
        content['userName']=user['userName']
        content['email']=user['email']
        content['roleId']=user['roleId']
        content['inWhiteList']=user['inWhiteList']
        return content
