export default {
  /* 每种角色id对应的类型 */
  roleTypeMap: {
    0: {
      roleName: '超管'
    },
    1: {
      roleName: '管理员'
    },
    2: {
      roleName: '评估方'
    },
    3: {
      roleName: '采购方'
    },
    4: {
      roleName: '供应方'
    }
  }
}
