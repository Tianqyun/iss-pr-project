export default {
  sidebarMap: {
    /* 角色和路由的关联 */
    0: {
      main: [
        {
          path: '/main/my_adminManage/GetAllUser',
          icon: 'orders-icon',
          title: '人员管理',
          group: 'my_adminManage',
          children: [
          ]},
        {
          path: '/main/my_statics/UserStatics',
          icon: 'suncaper-menu-unfold',
          title: '用户统计'
        }
      ]},
    1: {
      main: [
        {
          path: '/main/my_statics/UserStatics',
          icon: 'product-icon',
          title: '文字审核',
          group: 'product',
          children: []
        },
        {
          path: '/main/PictureList',
          icon: 'product-icon',
          title: '图片审核',
          group: 'product',
          children: []
        },
        {
          path: '/main/ContentList',
          icon: 'product-icon',
          title: '视频审核',
          group: 'product',
          children: []
        },
        {
          path: '/main/my_adminManage/GetWaitedOrders',
          icon: 'orders-icon',
          title: '订单审核'
        }
      ]
    },
    2: {
      main: [
        {
          path: '/main/UserInfo/EditUserIno',
          icon: 'edit-icon',
          title: '修改密码',
          group: 'UserInfo',
          children: []

        },
        {
          path: '/main/my_adminManage/GetWaitedProduct',
          icon: 'product-icon',
          title: '商品评估',
          group: 'product',
          children: []
        },
        {
          path: '/main/IM',
          icon: 'message-icon',
          title: '消息',
          group: 'IM',
          children: []
        }
      ]
    },
    3: {
      main: [
        {
          path: '/main/UserInfo',
          icon: 'settings-icon',
          title: '个人信息',
          group: 'batch',
          children: [
            {
              path: '/main/UserInfo/UserInfo',
              icon: 'userInfo-icon',
              title: '个人主页'
            },
            {
              path: '/main/UserInfo/EditUserIno',
              icon: 'edit-icon',
              title: '个人信息编辑'
            }
          ]
        },
        {
          path: '/main/my_product/ProductManage',
          icon: 'product-icon',
          title: '我的采购商品',
          group: 'product',
          children: []
        },
        {
          path: '/main/orders/myOrders',
          icon: 'orders-icon',
          title: '我的订单',
          group: 'orders',
          children: []
        },
        {
          path: '/main/IM',
          icon: 'message-icon',
          title: '消息',
          group: 'IM',
          children: []
        }
      ]
      // myPage: [
      //   {
      //     path: '/main/my_product/ProductAdd',
      //     icon: '',
      //     title: '添加商品'
      //   },
      //   {
      //     path: '/main/my_product/ProductDetail',
      //     icon: '',
      //     title: '商品详情'
      //   },
      //   {
      //     path: '/main/my_jingjia/CaigouGetJingjia',
      //     icon: '',
      //     title: '我的竞价'
      //   },
      //   {
      //     path: 'main/GetSearchUser',
      //     icon: '',
      //     title: '人员搜索结果'
      //   }
      // ]
    },
    4: {
      main: [
        {
          path: '/main/UserInfo',
          icon: 'settings-icon',
          title: '个人信息',
          group: 'batch',
          children: [
            {
              path: '/main/UserInfo/UserInfo',
              icon: 'userInfo-icon',
              title: '个人主页'
            },
            {
              path: '/main/UserInfo/EditUserIno',
              icon: 'edit-icon',
              title: '个人信息编辑'
            }
          ]
        },
        {
          path: '/main/my_product/ProductManage',
          icon: 'product-icon',
          title: '我的供应商品',
          group: 'product',
          children: []
        },
        {
          path: '/main/orders',
          icon: 'flow-icon',
          title: '订单管理',
          group: 'orders',
          children: [
            {
              path: '/main/my_jingjia/GongyingAllJingjia',
              icon: 'jingjia-icon',
              title: '我的竞价'
            },
            {
              path: '/main/orders/myOrders',
              icon: 'orders-icon',
              title: '我的订单'
            }
          ]
        },
        {
          path: '/main/IM',
          icon: 'message-icon',
          title: '消息',
          group: 'IM',
          children: []
        }
      ]
    }
  },
  sidebarTheme: {
    dark: {
      // '#1f2c35',
      background: '#1f2c35',
      text: '#ffffff',
      activeText: '#ffffff'
    },
    light: {
      background: '#ffffff',
      text: '#000000',
      activeText: '#1890ff'
    }
  }
}
