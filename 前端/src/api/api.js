import http from '@http/http'

/**
 * 检测api
 */
export const detectSentence = (input) => http.GET('/api/detectSentence/', {input})
/**
*  session相关
* */
// 前端获取用户信息
export const _getUserInfo = (errMsg) => http.GET('/api/web/getLoginUser/', {}, errMsg)
// 退出
export const userLogout = (errMsg) => http.GET('/api/auth/logout', {}, errMsg)

/**
* 登录注册
* */
// 用户名-密码登录
export const usernameLogin = (user) => http.GET('/api/web/userNameLogin/', user)

/*
* 内容相关
*/
export const getContentByTypeId = (typeId) => http.GET('/api/web/getContentByTypeId/', { typeId })

// 邮箱-验证码登录——发送验证码
export const getLoginVerCode = (email) => http.GET('/api/login/login/vercode', {email})
// 邮箱-验证码登录
export const emailVerifyCodeLogin = (email, verifyCode) => http.GET('/api/login/emailVerifyCodeLogin', {email, verifyCode})
// 注册界面获取地区信息
export const getAllAddresses = (errMsg) => http.GET('/api/login/getAllAddresses', {}, errMsg)
// 注册界面获取所有主营方向信息
export const getAllInterestIdName = (errMsg) => http.GET('/api/login/getAllInterestIdName', {}, errMsg)
// 注册界面获取验证码
export const getRegistVerCode = (email) => http.GET('/api/login/regist/vercode?email=' + email)
// 用户注册
export const regist = (registUser) => http.GET('/api/login/regist', registUser)
// 用户修改信息
export const editUserDetail = (user) => http.GET('/api/user/editUserDetail', user)
// removeLiscenseFromOBS
export const removeLiscenseFromOBS = (path) => http.POST('/api/login/removeLiscenseFromOBS', {path})
// getUserInterest 用户在修改信息时候获取的
export const getUserInterestWhenEdit = (userId) => http.GET('/api/user/getUserInterestWhenEdit', {userId})

/**
 * 首页展示商品
 * */
// 获取类别id
export const getMostLatestProductCatagoryId = () => http.GET('/api/product/getMostLatestProductCatagoryId')
// 获取相应类别的商品
export const showMostLatestProducts = (catagoryId, page) => http.GET('/api/product/showMostLatestProducts', {catagoryId, page})
// 分类查看商品 getProductByCatagory
export const getProductByCatagory = (pageNo, pageSize, catagoryId, roleId) => http.GET('/api/product/getProductByCatagory', {pageNo, pageSize, catagoryId, roleId})

/**
* 管理员功能
* */
// 管理员审核新用户
export const getRegistedUser = (page) => http.GET('/api/admin/getRegistedUser', page)
// 管理员审核新用户界面获取用户所在地区
export const getDictionaryAddress = (errMsg) => http.GET('/api/datadictionary/getDictionaryAddress', errMsg)
// 管理员审核时，获取用户主营方向
export const getUserInterest = (userId) => http.GET('/api/userinterest/getUserInterest?userId=' + userId)
// 新注册用户审核成功
export const makeUserLoginable = (userId) => http.GET('/api/admin/makeUserLoginable?userId=' + userId)
// 新注册用户审核失败
export const rejectRegistUser = (userId, description) => http.GET('/api/admin/rejectRegistUser', {description, userId})
// 管理员下载用户的营业执照以供审核
export const downloadFileFromOBS = (string) => http.GET('/api/admin/downloadFileFromOBS?string=' + string)
// 管理员获取评估方
export const getUserPinggu = (page) => http.GET('/api/admin/getUserPinggu', page)
// 管理员添加评估方
export const addUserPingguAndManager = (user) => http.POST('/api/admin/addUserPingguAndManager', user)
// 管理员删除评估方
export const removeUserPinggu = (id) => http.GET('/api/admin/removeUserPinggu?userId=' + id)
// 管理员审核订单 getWaitedOrders
export const getWaitedOrders = (page) => http.GET('/api/admin/getWaitedOrders', page)
// 获取供应方信息
export const getUsergongyingInfo = (ordersId) => http.GET('/api/admin/getUsergongyingInfo?ordersId=' + ordersId)
// 管理员对订单进行确认
export const confirmWaitedOrders = (ordersId, description) => http.GET('/api/admin/confirmWaitedOrders', {ordersId, description})
// 获取所有人员
export const getAllUser = (page) => http.GET('/api/admin/getAllUser', page)
// 获取审核记录
export const getAllUserShenhe = (page) => http.GET('/api/shenhehistory/getAllUserShenhe', page)

/**
 * 评估方
 * */
// 评估方对商品价格进行确认
export const getWaitedProduct = (pageNo, pageSize, categoryID) => http.GET('/api/userPinggu/getWaitedProduct', {pageNo, pageSize, categoryID})
// confirmWaitedProduct, rejectWaitedProduct
export const confirmWaitedProduct = (productId, description) => http.GET('/api/userPinggu/confirmWaitedProduct', {productId, description})
export const rejectWaitedProduct = (productId, description) => http.GET('/api/userPinggu/rejectWaitedProduct', {productId, description})

/**
 * 采购方、供应方
 * */
// 采购方，供应方 添加商品时展示的商品种类
export const getCatagoryList = (page) => http.POST('/api/catagory/getCatagoryList', page)
// 我的商品
export const myProduct = (pageNo, pageSize, categoryID) => http.GET('/api/product/myProduct', {pageNo, pageSize, categoryID})
// 获取全部评估方
export const getUserPingGuList = (errMsg) => http.GET('/api/product/getUserPingGuList', errMsg)
// 添加商品时，删除某一个图片
export const removePictureOnOBS = (path) => http.POST('/api/product/removePictureOnOBS?path=' + path)
// 添加商品
export const addProduct = (product) => http.POST('/api/product/addProduct', product)
// 商品详情
export const productDetail = (productId) => http.GET('/api/product/productDetail?productId=' + productId)
// 获取卖家信息 productSeller
export const productSeller = (productId) => http.GET('/api/product/productSeller?productId=' + productId)
// 采购方查看竞价
export const getProductCaigouJingjia = (page, productId) => http.GET('/api/jingjia/getProductCaigouJingjia', {page, productId})
// 我的订单
export const getMyOrders = (page) => http.GET('/api/orders/getMyOrders', page)
// getProductBySearch搜索商品
export const getProductBySearch = (userId, searchMethod, searchContent, categoryID, pageNo, pageSize) => http.GET('/api/search/getProductBySearch', {userId, searchMethod, searchContent, categoryID, pageNo, pageSize})
// getUserBySearch搜索人员
export const getUserBySearch = (searchMethod, searchContent, pageNo, pageSize) => http.GET('/api/search/getUserBySearch', {searchMethod, searchContent, pageNo, pageSize})
// userDetail
export const userDetail = (userId) => http.GET('/api/user/userDetail', {userId})
// editProduct
export const editProduct = (product) => http.GET('/api/product/editProduct', product)

/**
 * 订单
 * */
// acceptOrders采购方对 供应方商品下单，生成订单，然后返回订单号
export const acceptOrders = (productId, productNumber, ordersId, path) => http.GET('/api/orders/acceptOrders', {productId, productNumber, ordersId, path})
// aliPay 根据订单号来支付
export const aliPay = (ordersId) => http.GET('/api/orders/aliPay?ordersId=' + ordersId)
// 完成订单
export const finishOrders = (ordersId) => http.GET('/api/orders/finishOrders?ordersId=' + ordersId)

/**
* 竞价
* */
// 供应方竞价历史
export const getMyJingjiaHistory = (productId) => http.GET('/api/jingjia/getMyJingjiaHistory?productId=' + productId)
// 供应方竞价
export const insertJingjia = (productId, amount) => http.GET('/api/jingjia/insertJingjia', {productId, amount})
// 采购方接受竞价 acceptProductCaigouJingjia
export const acceptProductCaigouJingjia = (jingjiaId, ordersId, path) => http.GET('/api/jingjia/acceptProductCaigouJingjia', {jingjiaId, ordersId, path})
// getUserGongyingMyJingjias
export const getUserGongyingMyJingjias = (pageNo, pageSize) => http.GET('/api/jingjia/getUserGongyingMyJingjias', {pageNo, pageSize})

/**
 * 聊天
 * */
// userDetail
// export const userDetail = (userId) => http.GET('/api/user/userDetail', {userId})
// 获取可建立的会话列表，虽然没有什么用，但是还是写吧
export const getSessionListNot = (id) => http.GET('/api/sessionList/not', {id})
// 获取已存在的会话列表
export const sessionListAlready = (id) => http.GET('/api/sessionList/already', {id})
// 创建会话
export const createSession = (id, toUserId, toUserName) => http.GET('/api/createSession', {id, toUserId, toUserName})
// 删除会话 delSession
export const delSession = (sessionId) => http.GET('/api/delSession', {sessionId})
// 获取消息数据
export const msgList = (sessionId) => http.GET('/api/msgList', {sessionId})
// removeIMPictureFromOBS
export const removeIMPictureFromOBS = (path) => http.GET('/api/msgInfo/removeIMPictureFromOBS', {path})

/**
 * 可视化
 * */
// 用户统计 staticsUserRole
export const staticsUserRole = () => http.GET('/api/statics/staticsUserRole')
// staticsUserInterests
export const staticsUserInterests = () => http.GET('/api/statics/staticsUserInterests')

// 人员导出
export const exportUser = () => http.EXPORT('/api/admin/exportUser')
