const loginPage = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/Login',
    name: 'Login',
    component: () =>
      import(/* webpackChunkName: "Login" */ '../pages/main/routes/new_pages/Login')
  },
  {
    path: '/regist',
    name: 'regist',
    component: () =>
      import(/* webpackChunkName: "Login" */ '../pages/main/routes/new_pages/Regist')
  }
]

/* 一切需要登录访问的界面都卸载mainPage里边 */
const mainPage = [

  {
    path: '/main',
    name: 'main',
    component: () =>
      import(/* webpackChunkName: "Main" */ '../pages/main/Main'),
    children: [
      /* 这里配路由 */
      {
        path: 'PictureList',
        name: 'PictureList',
        component: () =>
          import(
            /* webpackChunkName: "First" */ '../pages/main/routes/new_pages/PictureList.vue'
          )
      },
      {
        path: 'ContentDetail',
        name: 'ContentDetail',
        component: () =>
          import(
            /* webpackChunkName: "First" */ '../pages/main/routes/new_pages/ContentDetail.vue'
          )

      },
      /// /////////////////////////////////////////////////////
      {
        path: 'Home',
        name: 'Home',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/Home'
            )
      },
      {
        path: 'first',
        name: 'first',
        component: () =>
            import(
              /* webpackChunkName: "Second" */ '../pages/main/routes/first/First')
      },
      {
        path: 'second/second',
        name: 'second',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/second/Second'
            )
      },
      {
        path: 'my_adminManage/getRegistedUser',
        name: 'GetRegistedUser',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_adminManage/GetRegistedUser'
            )
      },
      {
        path: 'my_product/ProductManage',
        name: 'ProductManage',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_product/ProductManage'
            )
      },
      {
        path: 'my_product/ProductAdd',
        name: 'ProductAdd',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_product/ProductAdd'
            )
      },
      {
        path: 'my_product/ProductDetail',
        name: 'ProductDetail',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_product/ProductDetail'
            )
      },
      {
        path: 'my_adminManage/GetUserPinggu',
        name: 'GetUserPinggu',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_adminManage/GetUserPinggu'
            )
      },
      {
        path: 'my_jingjia/CaigouGetJingjia',
        name: 'CaigouGetJingjia',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_jingjia/CaigouGetJingjia'
            )
      },
      {
        path: 'my_adminManage/GetWaitedProduct',
        name: 'GetWaitedProduct',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_adminManage/GetWaitedProduct'
            )
      },
      {
        path: 'orders/myOrders',
        name: 'myOrders',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/orders/myOrders'
            )
      },
      {
        path: 'my_adminManage/GetWaitedOrders',
        name: 'GetWaitedOrders',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_adminManage/GetWaitedOrders'
            )
      },
      {
        path: 'IM',
        name: 'IM',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/IM'
            )
      },
      {
        path: 'Goods',
        name: 'Goods',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_product/Goods'
            )
      },
      {
        path: 'GetSearchUser',
        name: 'GetSearchUser',
        component: () =>
                import(
                  /* webpackChunkName: "First" */ '../pages/main/routes/my_adminManage/GetSearchUser'
                )
      },
      {
        path: 'UserInfo/UserInfo',
        name: 'UserInfo',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/UserInfo/UserInfo'
            )
      },
      {
        path: 'my_statics/UserStatics',
        name: 'UserStatics',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_statics/UserStatics'
            )
      },
      {
        path: 'my_adminManage/GetAllUser',
        name: 'GetAllUser',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_adminManage/GetAllUser'
            )
      },

      {
        path: 'UserInfo/EditUserIno',
        name: 'EditUserIno',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/UserInfo/EditUserIno'
            )
      },
      {
        path: 'my_jingjia/GongyingAllJingjia',
        name: 'GongyingAllJingjia',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_jingjia/GongyingAllJingjia'
            )
      },
      {
        path: 'my_product/UserAllGoods',
        name: 'UserAllGoods',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_product/UserAllGoods'
            )
      },
      {
        path: 'my_product/EditProductInfo',
        name: 'EditProductInfo',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_product/EditProductInfo'
            )
      },
      {
        path: 'my_adminManage/GetAllUserShenhe',
        name: 'GetAllUserShenhe',
        component: () =>
            import(
              /* webpackChunkName: "First" */ '../pages/main/routes/my_adminManage/GetAllUserShenhe'
            )
      }
    ]
  }
]

const errorPage = [
  {
    path: '/notFound',
    name: 'notFound',
    component: () =>
      import(/* webpackChunkName: "NotFound" */ '../pages/error/NotFound')
  },
  {
    path: '/forbidden',
    name: 'forbidden',
    component: () =>
      import(/* webpackChunkName: "Forbidden" */ '../pages/error/Forbidden')
  },
  {
    path: '/badGateway',
    name: 'badGateway',
    component: () =>
      import(/* webpackChunkName: "BadGateway" */ '../pages/error/BadGateway')
  },
  {
    path: '*',
    redirect: '/notFound'
  }
]
export default [...loginPage, ...mainPage, ...errorPage]
