/*
 Navicat Premium Data Transfer

 Source Server         : project2
 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Host           : localhost:3307
 Source Schema         : project2

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 19/10/2022 20:29:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_group_permissions_group_id_permission_id_0cd325b0_uniq`(`group_id`, `permission_id`) USING BTREE,
  INDEX `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_permission_content_type_id_codename_01ab375a_uniq`(`content_type_id`, `codename`) USING BTREE,
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO `auth_permission` VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO `auth_permission` VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO `auth_permission` VALUES (4, 'Can view log entry', 1, 'view_logentry');
INSERT INTO `auth_permission` VALUES (5, 'Can add permission', 2, 'add_permission');
INSERT INTO `auth_permission` VALUES (6, 'Can change permission', 2, 'change_permission');
INSERT INTO `auth_permission` VALUES (7, 'Can delete permission', 2, 'delete_permission');
INSERT INTO `auth_permission` VALUES (8, 'Can view permission', 2, 'view_permission');
INSERT INTO `auth_permission` VALUES (9, 'Can add group', 3, 'add_group');
INSERT INTO `auth_permission` VALUES (10, 'Can change group', 3, 'change_group');
INSERT INTO `auth_permission` VALUES (11, 'Can delete group', 3, 'delete_group');
INSERT INTO `auth_permission` VALUES (12, 'Can view group', 3, 'view_group');
INSERT INTO `auth_permission` VALUES (13, 'Can add user', 4, 'add_user');
INSERT INTO `auth_permission` VALUES (14, 'Can change user', 4, 'change_user');
INSERT INTO `auth_permission` VALUES (15, 'Can delete user', 4, 'delete_user');
INSERT INTO `auth_permission` VALUES (16, 'Can view user', 4, 'view_user');
INSERT INTO `auth_permission` VALUES (17, 'Can add content type', 5, 'add_contenttype');
INSERT INTO `auth_permission` VALUES (18, 'Can change content type', 5, 'change_contenttype');
INSERT INTO `auth_permission` VALUES (19, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO `auth_permission` VALUES (20, 'Can view content type', 5, 'view_contenttype');
INSERT INTO `auth_permission` VALUES (21, 'Can add session', 6, 'add_session');
INSERT INTO `auth_permission` VALUES (22, 'Can change session', 6, 'change_session');
INSERT INTO `auth_permission` VALUES (23, 'Can delete session', 6, 'delete_session');
INSERT INTO `auth_permission` VALUES (24, 'Can view session', 6, 'view_session');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `last_login` datetime(6) NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `first_name` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `last_name` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(254) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_groups_user_id_group_id_94350c0c_uniq`(`user_id`, `group_id`) USING BTREE,
  INDEX `auth_user_groups_group_id_97559544_fk_auth_group_id`(`group_id`) USING BTREE,
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq`(`user_id`, `permission_id`) USING BTREE,
  INDEX `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for content
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content`  (
  `id` bigint(20) NOT NULL,
  `typeId` int(11) NOT NULL COMMENT '0文字，1图片，2视频',
  `userId` bigint(20) NOT NULL,
  `fromResource` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '内容来源：eg爱奇艺媒体',
  `mediaTitle` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '内容标题',
  `checkResult` int(11) NOT NULL COMMENT '检查结果 0未检查 -1不通过 1通过',
  `lableFromModel` int(11) NULL DEFAULT NULL COMMENT '-1/0 不通过 1通过',
  `url` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '整个内容的存储url',
  `frameUrl` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '视频对应展示帧的url',
  `strings` varchar(400) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '对应文本',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  `is_deleted` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Reference_2`(`userId`) USING BTREE,
  CONSTRAINT `FK_Reference_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of content
-- ----------------------------
INSERT INTO `content` VALUES (0, 1, 2, 'Netfilx', 'good good study', 0, NULL, 'https://backet-1.obs.cn-north-9.myhuaweicloud.com:443/1514406616680243202/2022-05-10+13:53:02_1514406616680243202ProductPictures.jfif', 'https://backet-1.obs.cn-north-9.myhuaweicloud.com:443/1514406616680243202/2022-05-10+13:53:02_1514406616680243202ProductPictures.jfif', NULL, '2022-10-19 15:21:02', '2022-10-19 15:21:06', 0);

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `object_repr` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `content_type_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `django_admin_log_content_type_id_c4bce8eb_fk_django_co`(`content_type_id`) USING BTREE,
  INDEX `django_admin_log_user_id_c564eba6_fk_auth_user_id`(`user_id`) USING BTREE,
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `model` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `django_content_type_app_label_model_76bd3d3b_uniq`(`app_label`, `model`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES (1, 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES (3, 'auth', 'group');
INSERT INTO `django_content_type` VALUES (2, 'auth', 'permission');
INSERT INTO `django_content_type` VALUES (4, 'auth', 'user');
INSERT INTO `django_content_type` VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES (6, 'sessions', 'session');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES (1, 'contenttypes', '0001_initial', '2022-10-19 04:22:42.133641');
INSERT INTO `django_migrations` VALUES (2, 'auth', '0001_initial', '2022-10-19 04:22:42.688902');
INSERT INTO `django_migrations` VALUES (3, 'admin', '0001_initial', '2022-10-19 04:22:42.816313');
INSERT INTO `django_migrations` VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2022-10-19 04:22:42.824218');
INSERT INTO `django_migrations` VALUES (5, 'admin', '0003_logentry_add_action_flag_choices', '2022-10-19 04:22:42.831226');
INSERT INTO `django_migrations` VALUES (6, 'contenttypes', '0002_remove_content_type_name', '2022-10-19 04:22:42.920802');
INSERT INTO `django_migrations` VALUES (7, 'auth', '0002_alter_permission_name_max_length', '2022-10-19 04:22:42.937746');
INSERT INTO `django_migrations` VALUES (8, 'auth', '0003_alter_user_email_max_length', '2022-10-19 04:22:42.953903');
INSERT INTO `django_migrations` VALUES (9, 'auth', '0004_alter_user_username_opts', '2022-10-19 04:22:42.961902');
INSERT INTO `django_migrations` VALUES (10, 'auth', '0005_alter_user_last_login_null', '2022-10-19 04:22:43.015773');
INSERT INTO `django_migrations` VALUES (11, 'auth', '0006_require_contenttypes_0002', '2022-10-19 04:22:43.018817');
INSERT INTO `django_migrations` VALUES (12, 'auth', '0007_alter_validators_add_error_messages', '2022-10-19 04:22:43.026817');
INSERT INTO `django_migrations` VALUES (13, 'auth', '0008_alter_user_username_max_length', '2022-10-19 04:22:43.043251');
INSERT INTO `django_migrations` VALUES (14, 'auth', '0009_alter_user_last_name_max_length', '2022-10-19 04:22:43.061368');
INSERT INTO `django_migrations` VALUES (15, 'auth', '0010_alter_group_name_max_length', '2022-10-19 04:22:43.077529');
INSERT INTO `django_migrations` VALUES (16, 'auth', '0011_update_proxy_permissions', '2022-10-19 04:22:43.084529');
INSERT INTO `django_migrations` VALUES (17, 'auth', '0012_alter_user_first_name_max_length', '2022-10-19 04:22:43.100528');
INSERT INTO `django_migrations` VALUES (18, 'sessions', '0001_initial', '2022-10-19 04:22:43.146964');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session`  (
  `session_key` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `session_data` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`) USING BTREE,
  INDEX `django_session_expire_date_a5c62663`(`expire_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO `django_session` VALUES ('3ae3tve3f2fw4959ev0ikme7t7y2o3og', '.eJyFjcsKwjAQRX8lzNpCHkgxfyCIq0I3AxLMgAOpLUm6Kv13SUp10YWzu-de5iwwJ4pgYUFgj2CFOgms8O4GKgBB1UMozeRS6sfojw0NjsOONxTHQNc6NSXyu39xphun_DVxengKlOknf0ZymTre9Vpq3SjZqIuQ0kpjzXl7P0_-z7BFWGH9AAEoPvA:1ol1L7:YV-v05bh5Jfc8r-aTaMnKBkM-FVVHvRk0yU9FZGsNJY', '2022-10-19 05:40:01.754897');
INSERT INTO `django_session` VALUES ('799zrvhp9lyff8q81z3nx0pf1sgr9awn', '.eJyFzTEKwzAMBdCrGM0NyA6l1DcolE6BLIJiakEFThNsZwq5e7FDyNChGt__fC0wJ45gYSEQT2CVPimq-HADFyDQ9QhKMrmU-jH634QHJ2HnjeIY-Hasyqd_S-a7pHxYenoOnLnWsNArssvcyf7eoDGNxkZfFaLF1rbnbX6e_J_ihWCF9Qv_yT7t:1ol3Jx:Nl0RNwHYykx6tHUm5Zvr3lAzgl8hm0j1STw3mCT_BJA', '2022-10-19 07:46:57.608590');
INSERT INTO `django_session` VALUES ('bohmvzxu69k7aq2d8iriael0e1ffgd3t', '.eJyFjcsKwjAQRX8lzNpCHkgxfyCIq0I3AxLMgAOpLUm6Kv13SUp10YWzu-de5iwwJ4pgYUFgj2CFOgms8O4GKgBB1UMozeRS6sfojw0NjsOONxTHQNc6NSXyu39xphun_DVxengKlOknf0ZymTre9Vpq3SjZqIuQ0kpjzXl7P0_-z7BFWGH9AAEoPvA:1ol1IP:T66ea_yscRr6moqRZjY08YgIAxJvkE_CUQbq1IbNDZ0', '2022-10-19 05:37:13.448004');
INSERT INTO `django_session` VALUES ('e8dofpfpcenajxdew2v8d5j1gbl84msm', '.eJyFzTEKwzAMBdCrGM0NyA6l1DcolE6BLIJiakEFThNsZwq5e7FDyNChGt__fC0wJ45gYSEQT2CVPimq-HADFyDQ9QhKMrmU-jH634QHJ2HnjeIY-Hasyqd_S-a7pHxYenoOnLnWsNArssvcyf7eoDGNxkZfFaLF1rbnbX6e_J_ihWCF9Qv_yT7t:1ol81Q:4WNVE5v6c7YSG26Jgsa--daQgN90mwwmPXhaTxR3m7s', '2022-10-19 12:48:08.126052');
INSERT INTO `django_session` VALUES ('gmngly8zqnz727aums0m9hl8dhvkqhta', '.eJyFzTEKwzAMBdCrGM0NyA6l1DcolE6BLIJiakEFThNsZwq5e7FDyNChGt__fC0wJ45gYSEQT2CVPimq-HADFyDQ9QhKMrmU-jH634QHJ2HnjeIY-Hasyqd_S-a7pHxYenoOnLnWsNArssvcyf7eoDGNxkZfFaLF1rbnbX6e_J_ihWCF9Qv_yT7t:1ol6ID:DZVhdnCHGT2mNGh-eKIcy16brQQ8jniAfblUDQDR3A8', '2022-10-19 10:57:21.313394');
INSERT INTO `django_session` VALUES ('h8j71km5ffuwhqe9cl8m2p0mylqqazy1', '.eJyFzTEKwzAMBdCrGM0NyA6l1DcolE6BLIJiakEFThNsZwq5e7FDyNChGt__fC0wJ45gYSEQT2CVPimq-HADFyDQ9QhKMrmU-jH634QHJ2HnjeIY-Hasyqd_S-a7pHxYenoOnLnWsNArssvcyf7eoDGNxkZfFaLF1rbnbX6e_J_ihWCF9Qv_yT7t:1ol810:aJsQWkN3GqBIW8F4DCW1eKxDJc3bVZotMdHlYA0KFQ8', '2022-10-19 12:47:42.221025');
INSERT INTO `django_session` VALUES ('ptvq0p91rxvnch72ptvo6vns7hw6dkuo', '.eJyFzTEKwzAMBdCrGM0NyA6l1DcolE6BLIJiakEFThNsZwq5e7FDyNChGt__fC0wJ45gYSEQT2CVPimq-HADFyDQ9QhKMrmU-jH634QHJ2HnjeIY-Hasyqd_S-a7pHxYenoOnLnWsNArssvcyf7eoDGNxkZfFaLF1rbnbX6e_J_ihWCF9Qv_yT7t:1ol58C:ZleRaXKaasf58k7pYGt6_PkhkmkVLeVJP1ZR1Hajeow', '2022-10-19 09:42:56.553282');
INSERT INTO `django_session` VALUES ('qyriww7b1k2vpyjr1c4cq191pplad2g0', '.eJyFjcsKwjAQRX8lzNpCHkgxfyCIq0I3AxLMgAOpLUm6Kv13SUp10YWzu-de5iwwJ4pgYUFgj2CFOgms8O4GKgBB1UMozeRS6sfojw0NjsOONxTHQNc6NSXyu39xphun_DVxengKlOknf0ZymTre9Vpq3SjZqIuQ0kpjzXl7P0_-z7BFWGH9AAEoPvA:1ol1I8:CXcM2zpiuptJ2SWrxMCvKAKanZXzO92_iLusehSha7k', '2022-10-19 05:36:56.904685');
INSERT INTO `django_session` VALUES ('tuipz8oeqfq9gsyxsdd2lvii0oqbvcj5', '.eJyFzTEKwzAMBdCrGM0NyA6l1DcolE6BLIJiakEFThNsZwq5e7FDyNChGt__fC0wJ45gYSEQT2CVPimq-HADFyDQ9QhKMrmU-jH634QHJ2HnjeIY-Hasyqd_S-a7pHxYenoOnLnWsNArssvcyf7eoDGNxkZfFaLF1rbnbX6e_J_ihWCF9Qv_yT7t:1ol7GB:d4C4viVzMSO9ty7IEXBgW7LDrZnRSNqLhEDGM_uj-wY', '2022-10-19 11:59:19.250010');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL,
  `userName` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `passWord` char(16) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `roleId` int(11) NULL DEFAULT NULL COMMENT '0超管，1管理，3上传者',
  `inWhiteList` int(11) NOT NULL COMMENT '0不在白名单，1在白名单',
  `is_deleted` int(1) NULL DEFAULT NULL COMMENT '0没有删除，1已删除',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '111111', '111111', '11', 1, 1, 0, '2022-10-19 00:03:35', '2022-10-19 00:03:37');
INSERT INTO `user` VALUES (2, 'uploader1', 'uploader1', '1', 3, 1, 0, '2022-10-19 15:09:20', '2022-10-19 15:09:26');

-- ----------------------------
-- Table structure for verifyhistory
-- ----------------------------
DROP TABLE IF EXISTS `verifyhistory`;
CREATE TABLE `verifyhistory`  (
  `id` bigint(20) NOT NULL,
  `userId` bigint(20) NULL DEFAULT NULL,
  `contentId` bigint(20) NULL DEFAULT NULL,
  `reason` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Reference_4`(`contentId`) USING BTREE,
  INDEX `FK_Reference_5`(`userId`) USING BTREE,
  CONSTRAINT `FK_Reference_4` FOREIGN KEY (`contentId`) REFERENCES `content` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Reference_5` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
